import DataService from '../../serveces/DataService';

export const namespaced = true;

export const state = {
  data: [],
};

export const mutations = {
  SET_EVENTS(state, data) {
    state.data = data;
  },
};

export const actions = {
  fetchData({ commit }) {
    DataService.getData().then(res => commit('SET_EVENTS', res.data.events));
  },
};
