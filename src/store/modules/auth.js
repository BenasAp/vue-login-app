import AuthService from '@/serveces/AuthService';

export const namespaced = true;

export const state = {
  user: null,
};

export const mutations = {
  SET_USER_DATA(state, userData) {
    state.user = userData;
    localStorage.setItem('user', JSON.stringify(userData));
    AuthService.setBearer(userData.token);
  },
  CLEAR_USER_DATA(state) {
    state.user = null;
    localStorage.removeItem('user');
    AuthService.clearBearer();
  },
};

export const actions = {
  register({ commit }, credentials) {
    return AuthService.registerUser(credentials).then(({ data }) => {
      commit('SET_USER_DATA', data);
    });
  },
  login({ commit }, credentials) {
    return AuthService.loginUser(credentials).then(({ data }) => {
      commit('SET_USER_DATA', data);
    });
  },
  logout({ commit }) {
    commit('CLEAR_USER_DATA');
  },
};

export const getters = {
  loggedIn(state) {
    return !!state.user;
  },
};
