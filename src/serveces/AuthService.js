import axios from 'axios';

export default {
  setBearer: token => {
    axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
  },
  clearBearer: () => {
    axios.defaults.headers.common = { Authorization: null };
  },
  registerUser: credentials =>
    axios.post('//localhost:3000/register', credentials),
  loginUser: credentials => axios.post('//localhost:3000/login', credentials),
};
