import axios from 'axios';

export default {
  getData() {
    return axios.get('//localhost:3000/data');
  },
};
