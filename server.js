require('dotenv').config();

const express = require('express');
const fs = require('fs');
const jwt = require('jsonwebtoken');
var cors = require('cors');
const bodyParser = require('body-parser');
const resData = require('./db/data.json');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.get('/data', authenticateToken, (req, res) => {
  jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, err => {
    if (err) {
      res.sendStatus(401);
    } else {
      res.json(resData);
    }
  });
});

app.post('/login', (req, res) => {
  const userDB = fs.readFileSync('./db/user.json');
  const userInfo = JSON.parse(userDB);

  if (
    req.body &&
    req.body.email === userInfo.email &&
    req.body.password === userInfo.password
  ) {
    const token = jwt.sign({ userInfo }, process.env.ACCESS_TOKEN_SECRET);
    res.json({
      token,
      email: userInfo.email,
      name: userInfo.name,
    });
  } else {
    res.send(401);
  }
});

app.post('/register', (req, res) => {
  if (req.body) {
    const user = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    };

    const data = JSON.stringify(user, null, 2);

    const dbUserEmail = require('./db/user.json').email;

    if (dbUserEmail === req.body.email) {
      res.sendStatus(400);
    } else {
      fs.writeFile('./db/user.json', data, err => {
        if (err) {
          console.log(err + data);
        } else {
          const token = jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET);
          res.json({
            token,
            email: user.email,
            name: user.name,
          });
        }
      });
    }
  } else {
    res.sendStatus(400);
  }
});

function authenticateToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  console.log(bearerHeader);

  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(401);
  }
}

app.listen(3000, () => {
  console.log('Server started on port 3000');
});
